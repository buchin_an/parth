package com.company.sorter;

import com.company.model.Worker;

import java.util.Comparator;

public class SortPaymentByMounth implements Comparator<Worker> {
    @Override
    public int compare(Worker o1, Worker o2) {
        return (int) (o1.getMonthPayment() - o2.getMonthPayment());
    }
}
