package com.company.sorter;

import com.company.model.Worker;

import java.util.Comparator;

public class SortByName implements Comparator<Worker> {
    @Override
    public int compare(Worker o1, Worker o2) {
        return o1.getName().compareTo(o2.getName());
    }
}
