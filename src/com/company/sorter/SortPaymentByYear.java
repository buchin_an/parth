package com.company.sorter;

import com.company.model.Worker;

import java.util.Comparator;

public class SortPaymentByYear implements Comparator<Worker> {
    @Override
    public int compare(Worker o1, Worker o2) {
        return (int) (o1.getYearPayment() - o2.getYearPayment());
    }
}
