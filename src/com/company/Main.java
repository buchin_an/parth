package com.company;

import com.company.model.Worker;
import com.company.sorter.SortByName;
import com.company.sorter.SortPaymentByMounth;
import com.company.sorter.SortPaymentByYear;

import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        List<Worker> workers = WorkerGenerator.generate();
        Scanner scanner = new Scanner(System.in);

        System.out.println("Choose sorting type");
        System.out.println("1 Sort by name");
        System.out.println("2 Sort by month payment");
        System.out.println("3 Sort by annual payment");

        int choise = Integer.parseInt(scanner.nextLine());

        if (choise == 1) {
            Collections.sort(workers, new SortByName());
        }
        if (choise == 2) {
            Collections.sort(workers, new SortPaymentByMounth());
        }
        if (choise == 3) {
            Collections.sort(workers, new SortPaymentByYear());
        }
        System.out.println(workers);
    }
}
