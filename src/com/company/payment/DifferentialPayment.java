package com.company.payment;

public class DifferentialPayment implements PaymentCalculation {
    double[] payment;

    public DifferentialPayment(double[] payment) {
        this.payment = payment;
    }


    @Override
    public double calculate() {
        return calculateDiff(payment);
    }

    private double calculateDiff(double[] payment) {
        double tmp = 0;
        for (int i = 0; i < payment.length; i++) {
            tmp += payment[i];
        }
        return tmp;
    }

}
