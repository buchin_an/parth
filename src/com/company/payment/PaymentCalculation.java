package com.company.payment;

public interface PaymentCalculation {
    double calculate();
}
