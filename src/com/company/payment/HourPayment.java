package com.company.payment;

public class HourPayment implements PaymentCalculation {
    private double hourPayment;
    private int workHour;
    private int workDay;

    public HourPayment(double hourPayment, int workHour, int workDay) {
        this.hourPayment = hourPayment;
        this.workHour = workHour;
        this.workDay = workDay;
    }

    @Override
    public double calculate() {
        return hourPayment * workHour * workDay;
    }
}
