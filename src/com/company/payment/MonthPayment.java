package com.company.payment;


public class MonthPayment implements PaymentCalculation {
    double monthPayment;

    public MonthPayment(double monthPayment) {
        this.monthPayment = monthPayment;
    }

    @Override
    public double calculate() {
        return monthPayment * 12;
    }
}
