package com.company.model;

import com.company.payment.PaymentCalculation;

public class Worker {

    private String name;
    private String post;
    private PaymentCalculation paymentCalculation;
    private double payment;


    public Worker(String name, String post) {
        this.name = name;
        this.post = post;

    }

    public void setPaymentCalculation(PaymentCalculation paymentCalculation) {
        this.paymentCalculation = paymentCalculation;
        payment = paymentCalculation.calculate();
    }

    public double getYearPayment() {
        return payment;
    }

    public double getMonthPayment() {
        return payment / 12;
    }

    public String getName() {
        return name;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "name='" + name + '\'' +
                ", post='" + post + '\'' +
                '}';
    }
}
