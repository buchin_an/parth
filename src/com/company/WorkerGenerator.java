package com.company;

import com.company.model.Worker;
import com.company.payment.DifferentialPayment;
import com.company.payment.HourPayment;
import com.company.payment.MonthPayment;

import java.util.Arrays;
import java.util.List;

public final class WorkerGenerator {

    public static List<Worker> generate() {

        Worker lena = new Worker("Иванова Елена Львовна", "зам директора");
        lena.setPaymentCalculation(new MonthPayment(9500));

        Worker dima = new Worker("Вакуленко Дмитрий Владимирович", "дизайнер");
        dima.setPaymentCalculation(new HourPayment(7, 8, 250));

        Worker anna = new Worker("Коренькова Анна Павловна", "менеджер по продажам");
        double[] annaPayment = {0.05 * 50000, 0.05 * 65000};
        anna.setPaymentCalculation(new DifferentialPayment(annaPayment));

        Worker tanya = new Worker("Татьяна Сергеевна", "менеджер по продажам");
        double[] tanyaPayment = {10000, 0.03 * 50000, 0.03 * 65000};
        tanya.setPaymentCalculation(new DifferentialPayment(tanyaPayment));

        return Arrays.asList(lena, dima, anna, tanya);
    }
}
